# Parallel computing: Edge detection with **CUDA**


## Sobel Operator: Edge Detection

Identifying points in a digital image at which the image brightness changes sharply.

<img src="./samples/3.png" width="200"> <img src="./samples/3.out.png" width="200"> 


## Goal

Given a sequential version of sample code, [parallelize it with CUDA](./hw3.cu)

## Usage

1. Compile and execute the source code
    ```
    $ make
    $ ./hw3 in.png out.png
    ```
    where `in.png` is the input image and `out.png` is the file name of the output image


**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
