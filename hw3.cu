#include <png.h>
#include <zlib.h>

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <iostream>

#define MASK_N 2
#define MASK_X 5
#define MASK_Y 5
#define SCALE 8

#define CHUNCK_LEN 128
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

/*
Device 1: "GeForce GTX 1080"
  CUDA Driver Version / Runtime Version          11.0 / 11.0
  CUDA Capability Major/Minor version number:    6.1
  Total amount of global memory:                 8118 MBytes (8511881216 bytes)
  (20) Multiprocessors, (128) CUDA Cores/MP:     2560 CUDA Cores
  Memory Bus Width:                              256-bit
  L2 Cache Size:                                 2097152 bytes
  Total amount of shared memory per block:       49152 bytes
  Total number of registers available per block: 65536
  Warp size:                                     32
  Maximum number of threads per multiprocessor:  2048
  Maximum number of threads per block:           1024
  Max dimension size of a thread block (x,y,z): (1024, 1024, 64)
  Max dimension size of a grid size    (x,y,z): (2147483647, 65535, 65535)
  Maximum memory pitch:                          2147483647 bytes
*/

__global__
void sobel(unsigned char* s, unsigned char* t, unsigned height, unsigned width, unsigned channels, int slabIdx) {

    int adjustX = (MASK_X % 2) ? 1 : 0;
    int adjustY = (MASK_Y % 2) ? 1 : 0;
    int xBound = MASK_X / 2;
    int yBound = MASK_Y / 2;

    int tidx = blockIdx.x * (CHUNCK_LEN-2*xBound) + threadIdx.x; 
    int tidy = slabIdx * (CHUNCK_LEN-2*yBound) + threadIdx.y;

    char mask[MASK_N][MASK_X][MASK_Y] = {
        {{ -1, -4, -6, -4, -1},
         { -2, -8,-12, -8, -2},
         {  0,  0,  0,  0,  0},
         {  2,  8, 12,  8,  2},
         {  1,  4,  6,  4,  1}},

        {{ -1, -2,  0,  2,  1},
         { -4, -8,  0,  8,  4},
         { -6,-12,  0, 12,  6},
         { -4, -8,  0,  8,  4},
         { -1, -2,  0,  2,  1}}
    };

    __shared__ unsigned char Pixels[CHUNCK_LEN*CHUNCK_LEN*3];
    int x_base = blockIdx.x * (CHUNCK_LEN-2*xBound) - xBound; // Pixels[0,0] = s[y_base, x_base]
    int y_base = slabIdx * (CHUNCK_LEN-2*yBound) - yBound; 

    // copy pixels to shared memory
    int x_from = tidx-xBound;
    int y_from = tidy-yBound;
    int x_to = MIN(x_base + CHUNCK_LEN, width);
    int y_to = MIN(y_base + CHUNCK_LEN, height);
    for (int y = y_from; y < y_to; y+=blockDim.y){
        for (int x = x_from; x < x_to; x+=blockDim.x){
            if (x >= 0 && y >= 0) {
                Pixels[channels*(CHUNCK_LEN*(y-y_base)+(x-x_base)) + 2] = s[channels*(width * y + x) + 2];
                Pixels[channels*(CHUNCK_LEN*(y-y_base)+(x-x_base)) + 1] = s[channels*(width * y + x) + 1];
                Pixels[channels*(CHUNCK_LEN*(y-y_base)+(x-x_base)) + 0] = s[channels*(width * y + x) + 0];
            }
        }
    }

    __syncthreads();

    // convolution
    x_from = tidx;
    y_from = tidy;
    x_to = MIN(blockIdx.x * (CHUNCK_LEN-2*xBound) + (CHUNCK_LEN-2*xBound), width);
    y_to = MIN(slabIdx * (CHUNCK_LEN-2*yBound) + (CHUNCK_LEN-2*yBound), height);

    for (int y = y_from; y < y_to; y+=blockDim.y) {
        for (int x = x_from; x < x_to; x+=blockDim.x) {

            float val[MASK_N * 3] = {0.0};
            for (int i = 0; i < MASK_N; ++i) {

                val[i * 3 + 2] = 0.0;
                val[i * 3 + 1] = 0.0;
                val[i * 3] = 0.0;

                // cal inner product of a mask and an output pixel 
                for (int v = -yBound; v < yBound + adjustY; ++v) {
                    for (int u = -xBound; u < xBound + adjustX; ++u) {
                        if ((x + u) >= 0 && (x + u) < width && y + v >= 0 && y + v < height) {
                            int R = Pixels[channels * (CHUNCK_LEN * (y + v - y_base) + (x + u - x_base)) + 2];
                            int G = Pixels[channels * (CHUNCK_LEN * (y + v - y_base) + (x + u - x_base)) + 1];
                            int B = Pixels[channels * (CHUNCK_LEN * (y + v - y_base) + (x + u - x_base)) + 0];
                            val[i * 3 + 2] += R * mask[i][u + xBound][v + yBound];
                            val[i * 3 + 1] += G * mask[i][u + xBound][v + yBound];
                            val[i * 3 + 0] += B * mask[i][u + xBound][v + yBound];
                        }
                    }
                }
            }

            // cal totalR, totalG, totalB
            float totalR = 0.0;
            float totalG = 0.0;
            float totalB = 0.0;
            for (int i = 0; i < MASK_N; ++i) {
                totalR += val[i * 3 + 2] * val[i * 3 + 2];
                totalG += val[i * 3 + 1] * val[i * 3 + 1];
                totalB += val[i * 3 + 0] * val[i * 3 + 0];
            }
            totalR = sqrt(totalR) / SCALE;
            totalG = sqrt(totalG) / SCALE;
            totalB = sqrt(totalB) / SCALE;

            // write output
            const unsigned char cR = (totalR > 255.0) ? 255 : totalR;
            const unsigned char cG = (totalG > 255.0) ? 255 : totalG;
            const unsigned char cB = (totalB > 255.0) ? 255 : totalB;
            t[channels * (width * y + x) + 2] = cR;
            t[channels * (width * y + x) + 1] = cG;
            t[channels * (width * y + x) + 0] = cB;

        }
    }
}

int read_png(const char* filename, unsigned char** image, unsigned* height, unsigned* width,
    unsigned* channels) {
    unsigned char sig[8];
    FILE* infile;
    infile = fopen(filename, "rb");

    fread(sig, 1, 8, infile);
    if (!png_check_sig(sig, 8)) return 1; /* bad signature */

    png_structp png_ptr;
    png_infop info_ptr;

    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) return 4; /* out of memory */

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        return 4; /* out of memory */
    }

    png_init_io(png_ptr, infile);
    png_set_sig_bytes(png_ptr, 8);
    png_read_info(png_ptr, info_ptr);
    int bit_depth, color_type;
    png_get_IHDR(png_ptr, info_ptr, width, height, &bit_depth, &color_type, NULL, NULL, NULL);

    png_uint_32 i, rowbytes;
    png_bytep row_pointers[*height];
    png_read_update_info(png_ptr, info_ptr);
    rowbytes = png_get_rowbytes(png_ptr, info_ptr);
    *channels = (int)png_get_channels(png_ptr, info_ptr);

    cudaMallocHost(image, rowbytes * *height);

    for (i = 0; i < *height; ++i) {
        row_pointers[i] = *image + i * rowbytes;
    }

    png_read_image(png_ptr, row_pointers);
    png_read_end(png_ptr, NULL);
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    return 0;
}

void write_png(const char* filename, png_bytep image, const unsigned height, const unsigned width,
    const unsigned channels) {
    FILE* fp = fopen(filename, "wb");
    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_infop info_ptr = png_create_info_struct(png_ptr);
    png_init_io(png_ptr, fp);
    png_set_IHDR(png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    png_set_filter(png_ptr, 0, PNG_NO_FILTERS);
    png_write_info(png_ptr, info_ptr);
    png_set_compression_level(png_ptr, 0);

    png_bytep row_ptr[height];
    for (int i = 0; i < height; ++i) {
        row_ptr[i] = image + i * width * channels * sizeof(unsigned char);
    }
    png_write_image(png_ptr, row_ptr);
    png_write_end(png_ptr, NULL);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);
}


int main(int argc, char** argv) {
    assert(argc == 3);

    // read img
    unsigned height, width, channels;
    unsigned char* src_img = NULL;
    read_png(argv[1], &src_img, &height, &width, &channels);
    assert(channels == 3);
    size_t imgSize = height * width * channels * sizeof(unsigned char);

    // malloc
    unsigned char* dst_img;
    unsigned char *src_img_dev, *dst_img_dev;
    cudaMallocHost(&dst_img, imgSize);
    cudaMalloc(&src_img_dev, imgSize);
    cudaMalloc(&dst_img_dev, imgSize);



    int xBound = MASK_X / 2;
    int yBound = MASK_Y / 2;
    int numSlabs = (height+(CHUNCK_LEN-2*yBound-1))/(CHUNCK_LEN-2*yBound); // each slab a stream

    for (int slabIdx=0; slabIdx < numSlabs; slabIdx++){

        cudaStream_t stream;
        cudaStreamCreate(&stream);

        // copy data to gpu
        int copyFrom_src = MAX( slabIdx*(CHUNCK_LEN-2*yBound)-2 , 0 ) * width * channels;
        int copyTo_src = MIN( (slabIdx+1)*(CHUNCK_LEN-2*yBound)+2 , height) * width * channels;
        int copySize_src = copyTo_src - copyFrom_src;
        cudaMemcpyAsync(&src_img_dev[copyFrom_src], 
                   &src_img[copyFrom_src], 
                   copySize_src,
                   cudaMemcpyHostToDevice,
                   stream);
        
        // call kernel 
        dim3 numBlocks((width+(CHUNCK_LEN-2*xBound-1))/(CHUNCK_LEN-2*xBound), 1, 1);
        dim3 threadsPerBlock(32, 32, 1);
        sobel<<<numBlocks, threadsPerBlock, 0, stream>>>(src_img_dev, dst_img_dev, height, width, channels, slabIdx);

        // copy data back to main mem 
        int copyFrom_dst = slabIdx*(CHUNCK_LEN-2*yBound) * width * channels;
        int copyTo_dst = MIN( (slabIdx+1)*(CHUNCK_LEN-2*yBound) , height) * width * channels;
        int copySize_dst = copyTo_dst - copyFrom_dst;
        cudaMemcpyAsync(&dst_img[copyFrom_dst],
                        &dst_img_dev[copyFrom_dst],
                        copySize_dst,
                        cudaMemcpyDeviceToHost,
                        stream);


        cudaStreamDestroy(stream);
    }




    // write png
    cudaDeviceSynchronize();
    write_png(argv[2], dst_img, height, width, channels);

    // free mem
    cudaFreeHost(src_img);
    cudaFreeHost(dst_img);
    cudaFree(src_img_dev);
    cudaFree(dst_img_dev);

    return 0;
}
